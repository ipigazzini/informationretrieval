package unimib.ilaria.pigazzini.ir.project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.StandardDirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.Token;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermInSetQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopDocsCollector;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.BytesRefArray;
import org.apache.lucene.util.Counter;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimib.ilaria.pigazzini.ir.project.filtering.EventFilter;
import unimib.ilaria.pigazzini.ir.project.filtering.UserProfile;
import unimib.ilaria.pigazzini.ir.project.filtering.UserProfiler;
import unimib.ilaria.pigazzini.ir.project.indexing.EventAnalyzer;
import unimib.ilaria.pigazzini.ir.project.indexing.FieldEnum;
import unimib.ilaria.pigazzini.ir.project.indexing.GatherAndParseDocs;
import unimib.ilaria.pigazzini.ir.project.indexing.IndexCreator;

public class IndexCreationTest {
	private final Logger logger = LoggerFactory.getLogger(IndexCreationTest.class);

	
	private File file = new File(
			"C:\\Users\\ipiga\\Desktop\\Università\\Information Retrieval\\Progetto\\facebook_events_crawler\\data\\Milano\\2017-11-01\\events-3.json");
	private File fileLinux = new File(
			"/home/ilaria/Documents/Uni/Information Retrieval/Progetto/facebook_events_crawler/data/Milano/2017-11-01/events-3.json");
	private String url = "C:\\Users\\ipiga\\Desktop\\Università\\Information Retrieval\\Progetto\\facebook_events_crawler\\data\\";
	private Path urlLinux = Paths.get("home", "ilaria", "Documents", "Uni", "Information Retrieval", "Progetto", "facebook_events_crawler", "data");
	private Path urlLinux2 = Paths.get("/home/ilaria/Documents/Uni/Information Retrieval/Progetto/facebook_events_crawler/data/");
	private File veg1File = new File(getClass().getClassLoader().getResource("user1/veg1.txt").getFile());
//	private Path nicolettaUrl = Paths.get("home", "ilaria", "git", "informationretrieval","src", "resources", "user1"); 
	private Path nicolettaUrl = Paths.get("./src/resources/user1"); 
	private Path index = Paths.get("indexes");
	
	
	public void testAnalyzer() {

		try {
			GatherAndParseDocs gatherer = new GatherAndParseDocs();

			IndexCreator ir = new IndexCreator("indexes");
			ir.createIndex(gatherer.parse(fileLinux));
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void testWithAllDocs() {
		GatherAndParseDocs gatherer = new GatherAndParseDocs();

		IndexCreator ir = new IndexCreator("indexes");

		try {
			ir.createIndex(gatherer.parse(urlLinux));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void testAnalyzerWithoutIndexCreator() {
//		EventAnalyzer analyzer = new EventAnalyzer();
//		DirectoryReader reader = StandardDirectoryReader.open(writer);
//		reader = new FileReader(veg1File);
//		TokenStream ts = analyzer.tokenStream(FieldEnum.FIELD_EVENT_DESCRIPTION.toString(), reader);
//		List<Token> tokens = Arrays.asList(ts);
//		
//		IndexReader indexReader = DirectoryReader.open(); 
//		TermEnum termEnum = indexReader.terms(); 
//		while (termEnum.next()) { 
//		    Term term = termEnum.term(); 
//		    System.out.println(term.text()); 
//		}
//		termEnum.close(); 
//		indexReader.close(); 

	}
	
	//@Test
	public void testModel() {
		String eventsPath = "indexes/eventsIndex";
		String userPath = "indexes/user1Index";
		
		//cleanIndexes(eventsPath, userPath);
		
		GatherAndParseDocs gatherer = new GatherAndParseDocs();

		IndexCreator indexCreator = new IndexCreator(eventsPath);

		try {
			
			//IndexReader reader = ir.createIndex(gatherer.parse(urlLinux));
			Directory indexDirectory = FSDirectory.open(Paths.get(eventsPath));
			IndexReader reader = DirectoryReader.open(indexDirectory);

			indexCreator.setIndexPath(userPath);
			// IndexReader userReader = indexCreator.createIndex(gatherer.parse(nicolettaUrl));
			Directory userIndexDirectory = FSDirectory.open(Paths.get(userPath));
			IndexReader userReader = DirectoryReader.open(userIndexDirectory);
			
			IndexSearcher eventSearcher = new IndexSearcher(reader);
			eventSearcher.setSimilarity(new ClassicSimilarity());

			EventFilter filter = new EventFilter(eventSearcher);
			
			UserProfiler profiler = new UserProfiler();
			
			List<BytesRef> terms = profiler.getUserProfile(userReader, FieldEnum.FIELD_USER_CONTENT.toString());
			
			UserProfile profile = new UserProfile();
			
			//printTerms(terms);
			List<BytesRef> controlledTerms = profile.getCiboTerms();

			
			Query query = new TermInSetQuery(FieldEnum.FIELD_EVENT_DESCRIPTION.toString(), controlledTerms);
			
			//Query query = new TermInSetQuery(FieldEnum.FIELD_EVENT_DESCRIPTION.toString(), terms);//new BytesRef("vegetarian"));
			
			filter.search(query);
			
					
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCleanDuplicates() {
		GatherAndParseDocs gatherer = new GatherAndParseDocs();
		List<Document> docs = gatherer.parse(urlLinux2);
		

		for(Document doc : docs) {
			logger.debug("PRE_ID --- " + doc.get(FieldEnum.FIELD_EVENT_ID.toString()));
		}
		
		gatherer.cleanDuplicates(docs);
		
		for(Document doc : docs) {
			logger.debug("POST_ID --- " + doc.get(FieldEnum.FIELD_EVENT_ID.toString()));
		}
	}
	
	public void printTerms(Terms terms) throws IOException {
		TermsEnum te = terms.iterator();
		while(te.next() != null) {
			System.out.println(te.term().utf8ToString());
		}
	}
	

	
	public void cleanIndexes(String ...paths) {
		File f;
		for (String path : paths) {
			try {
				f = new File(path);
				for (File file : f.listFiles()) {
					Files.delete(file.toPath());
				}
				logger.info("Deleted all files in " + path);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}; 
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
