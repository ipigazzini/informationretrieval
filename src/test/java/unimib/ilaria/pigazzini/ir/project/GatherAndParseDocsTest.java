package unimib.ilaria.pigazzini.ir.project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.document.Document;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimib.ilaria.pigazzini.ir.project.indexing.GatherAndParseDocs;
import unimib.ilaria.pigazzini.ir.project.indexing.IndexCreator;


public class GatherAndParseDocsTest {
	private final Logger logger = LoggerFactory.getLogger(GatherAndParseDocsTest.class);

	@Test
	public void parseTest() {
		File file = new File(
				"/home/ilaria/Documents/Uni/Information Retrieval/Progetto/facebook_events_crawler/data/Milano/2017-11-01/events-3.json");
		GatherAndParseDocs parser = new GatherAndParseDocs();
		try {
			List<Document> docs = parser.parse(file);
			logger.info("num docs: " + docs.size());

			IndexCreator icreator = new IndexCreator("indexes/eventsIndex");
			try {
				icreator.createIndex(docs);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			e.getMessage();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
