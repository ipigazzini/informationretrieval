package exercises.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.junit.Test;

import exercises.exercise1.AmazonBooks;
import junit.framework.TestCase;

public class exercise1 {
	private static List<Document> docs = new ArrayList<Document>();
	
	@Test
	public void addFieldToBooksTest() {
		Document book1 = new Document();
		Document book2 = new Document();
		Document book3 = new Document();
		Document book4 = new Document();
		Document book5 = new Document();
		
		List<Field> book1Fields = new ArrayList();
		
		book1Fields.add(AmazonBooks.createFieldAuthor("Isaac Asimov"));
		book1Fields.add(AmazonBooks.createFieldTitle("The Caves of Steel"));
		book1Fields.add(AmazonBooks.createFieldIsbn("0-553-29340-0"));
		book1Fields.add(AmazonBooks.createFieldPrice(10.00));
		book1Fields.add(AmazonBooks.createFieldDescription("First book of the Robot cycle"));
		book1Fields.add(AmazonBooks.createFieldNumOfPages(224));
		
		book1 = AmazonBooks.addFields(book1, book1Fields);

		List<Field> book2Fields = new ArrayList();
		book2Fields.add(AmazonBooks.createFieldAuthor("Jane Austen"));
		book2Fields.add(AmazonBooks.createFieldTitle("Pride and Prejudice"));
		book2Fields.add(AmazonBooks.createFieldIsbn("0-553-29340-0"));
		book2Fields.add(AmazonBooks.createFieldPrice(10.00));
		book2Fields.add(AmazonBooks.createFieldDescription("Best book of the british wrtiter"));
		book2Fields.add(AmazonBooks.createFieldNumOfPages(224));
		
		book2 = AmazonBooks.addFields(book2, book2Fields);

		List<Field> book3Fields = new ArrayList();
		book3Fields.add(AmazonBooks.createFieldAuthor("Chuck Palahniuk"));
		book3Fields.add(AmazonBooks.createFieldTitle("Fight Club"));
		book3Fields.add(AmazonBooks.createFieldIsbn("0-553-29340-0"));
		book3Fields.add(AmazonBooks.createFieldPrice(10.00));
		book3Fields.add(AmazonBooks.createFieldDescription("Harsh and Sharp, one of the best book by Palahniuk"));
		book3Fields.add(AmazonBooks.createFieldNumOfPages(224));

		book3 = AmazonBooks.addFields(book3, book3Fields);
		
		List<Field> book4Fields = new ArrayList();
		book4Fields.add(AmazonBooks.createFieldAuthor("Milan Kundera"));
		book4Fields.add(AmazonBooks.createFieldTitle("Immortality"));
		book4Fields.add(AmazonBooks.createFieldIsbn("0-553-29340-0"));
		book4Fields.add(AmazonBooks.createFieldPrice(10.00));
		book4Fields.add(AmazonBooks.createFieldDescription("This book is for people with very sad love stories"));
		book4Fields.add(AmazonBooks.createFieldNumOfPages(224));
		
		book4 = AmazonBooks.addFields(book4, book4Fields);

		List<Field> book5Fields = new ArrayList();
		book5Fields.add(AmazonBooks.createFieldAuthor("Daniel Pennac"));
		book5Fields.add(AmazonBooks.createFieldTitle("Monster Paradise"));
		book5Fields.add(AmazonBooks.createFieldIsbn("0-553-29340-0"));
		book5Fields.add(AmazonBooks.createFieldPrice(10.00));
		book5Fields.add(AmazonBooks.createFieldDescription("The first volume of the Malaussene cycle"));
		book5Fields.add(AmazonBooks.createFieldNumOfPages(224));
		
		book5 = AmazonBooks.addFields(book5, book5Fields);

		
		docs.add(book1);
		docs.add(book2);
		docs.add(book3);
		docs.add(book4);
		docs.add(book5);

		
	}
	
	@Test
	public void createIndexTest() {
		AmazonBooks.createIndex(docs);
	}
}
