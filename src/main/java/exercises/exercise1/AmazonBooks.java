package exercises.exercise1;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class AmazonBooks {
	
	public static Field createFieldIsbn(String isbn) {
		return new StringField("isbn", isbn, Field.Store.YES);
	}

	public static Field createFieldAuthor(String author) {
		return new TextField("author", author, Field.Store.YES);

	}
	
	public static Field createFieldTitle(String title) {
		return new TextField("title", title, Field.Store.YES);

	}
	
	public static Field createFieldDescription(String description) {
		return new TextField("description", description, Field.Store.YES);
	}
	
	public static Field createFieldNumOfPages(int numberOfPages) {
		return new StoredField("numberOfPages", numberOfPages);
	}
	
	public static Field createFieldPrice(double price) {
		return new StoredField("price", price);

	}
	
	public static Document addFields(Document book, List<Field> bookFields) {
		for(Field f : bookFields) {
			book.add(f);
		}
		
		return book;
	}
	
	public static void createIndex(List<Document> docs) {
		try {
			addDocsToIndex(docs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void addDocsToIndex(List<Document> docs) throws IOException {

		StandardAnalyzer analyzer = new StandardAnalyzer();
		Path path = Paths.get("indexes");
		Directory directory = FSDirectory.open(path);
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter iwriter = new IndexWriter(directory,config);

		for(Document doc : docs) {
			iwriter.addDocument(doc);
		}

		iwriter.close();
	}
	
	

}
