package unimib.ilaria.pigazzini.ir.project.filtering;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimib.ilaria.pigazzini.ir.project.indexing.FieldEnum;

public class UserProfiler {
	private final Logger logger = LoggerFactory.getLogger(UserProfiler.class);
	
	public UserProfiler() {

	}

	
	public void getUserProfile(String userId) {
		
	}
	
	public List<BytesRef> getUserProfile(IndexReader userReader, String userField) throws IOException {
		Terms terms = MultiFields.getTerms(userReader, userField);
		System.out.println("terms: " + terms);
		//ir.printTerms(terms);
		return getBytesRefArray(terms);
	}
	
	public List<BytesRef> getBytesRefArray(Terms terms) throws IOException {
		TermsEnum te = terms.iterator();
		List <BytesRef> arrayb = new ArrayList<>();
		while(te.next() != null) {
			System.out.println(te.term().utf8ToString());
			arrayb.add(te.term());
		}
		
		return arrayb;
	}
}
