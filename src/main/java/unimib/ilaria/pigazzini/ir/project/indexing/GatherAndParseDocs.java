package unimib.ilaria.pigazzini.ir.project.indexing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexableField;
import org.json.*;

public class GatherAndParseDocs {
	private final Logger logger = LoggerFactory.getLogger(GatherAndParseDocs.class);

	public GatherAndParseDocs() {

	}

	public List<Document> parse(Path systemPath) {
		List<Document> docs = new ArrayList<>();
		Stream<Path> stream;
		try {
			stream = Files.walk(systemPath);
			stream.forEach(filePath -> {
				// logger.debug("Is regular file: " + Files.isRegularFile(filePath) + ", " +
				// filePath);
				if (Files.isRegularFile(filePath)) {
					try {
						if (filePath.toString().lastIndexOf("json") != -1
								|| filePath.toString().lastIndexOf("txt") != -1) {

							docs.addAll(parse(filePath.toFile()));
						}
					} catch (Exception e) {
						logger.debug(e.getMessage());
					}
				}
			});
			stream.close();
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		this.cleanDuplicates(docs);
		return docs;
	}

	public List<Document> cleanDuplicates(List<Document> docs) {
		List<String> uniqueIds = new ArrayList<>();
		Iterator<Document> i = docs.iterator();
		if (i.hasNext() && i.next().getField(FieldEnum.FIELD_EVENT_ID.toString()) != null) {
			while (i.hasNext()) {
				IndexableField id = i.next().getField(FieldEnum.FIELD_EVENT_ID.toString());
				if (uniqueIds.contains(id.stringValue())) {
					i.remove();
				} else {
					uniqueIds.add(id.stringValue());
				}
			}
		}
		return docs;
	}

	public List<Document> parse(File file) throws IOException {
		List<Document> documents = new ArrayList<Document>();

		FieldType newType = new FieldType();
		newType.setStored(true);
		newType.setTokenized(true);
		newType.setStoreTermVectors(true);
		newType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);

		if (file.getName().lastIndexOf("json") != -1) {
			FileReader reader = new FileReader(file);

			JSONTokener tokener = new JSONTokener(reader);
			JSONObject obj = new JSONObject(tokener);
			JSONArray arr = obj.getJSONArray("events");

			for (int i = 0; i < arr.length(); i++) {
				Document doc = new Document();
				try {
					String event_id = arr.getJSONObject(i).getString("id");
					doc.add(new Field(FieldEnum.FIELD_EVENT_ID.toString(), event_id, newType));
					// logger.debug("event_id: " + event_id);

					String event_name = arr.getJSONObject(i).getString("name");
					doc.add(new Field(FieldEnum.FIELD_EVENT_NAME.toString(), event_name, newType));
					// logger.debug("event_name: " + event_name);

					String event_description = arr.getJSONObject(i).getString("description");
					doc.add(new Field(FieldEnum.FIELD_EVENT_DESCRIPTION.toString(), event_description, newType));
					// logger.debug("event_description: " + event_description);

				} catch (JSONException e) {
					e.getMessage();
					logger.error("no description found");
				} finally {
					documents.add(doc);
				}

			}
		} else {
			byte[] encoded = Files.readAllBytes(file.toPath());
			String user_content = new String(encoded, StandardCharsets.UTF_8);
			// logger.debug("user_content: " + user_content);
			String user_category = "cibo";// user_content.substring(0, space_index);
			logger.debug("category: " + user_category);
			Document doc = new Document();
			doc.add(new Field(FieldEnum.FIELD_USER_CONTENT.toString(), user_content, newType));
			doc.add(new Field(FieldEnum.FIELD_USER_CATEGORY.toString(), user_category, newType));

			documents.add(doc);
		}

		return documents;
	}

}
