package unimib.ilaria.pigazzini.ir.project.indexing;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.StandardDirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexCreator {
	private final Logger logger = LoggerFactory.getLogger(IndexCreator.class);
	private String indexPath = "";
	
	public IndexCreator(String indexPath) {
		this.indexPath = indexPath;
	}
	
	
	public IndexReader createIndex(List<Document> docs) throws IOException {

		EventAnalyzer analyzer = new EventAnalyzer();
		//ItalianAnalyzer analyzer = new ItalianAnalyzer();
		Path path = Paths.get(indexPath);
		Directory directory = FSDirectory.open(path);
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		config.setSimilarity(new ClassicSimilarity());
		IndexWriter iwriter = new IndexWriter(directory,config);

		for(Document doc : docs) {
			iwriter.addDocument(doc);
		}
		
		IndexReader reader = StandardDirectoryReader.open(iwriter);
	
		iwriter.close();
//		Terms terms = MultiFields.getTerms(reader, FieldEnum.FIELD_EVENT_DESCRIPTION.toString());
//		
//
//		printTerms(terms);
			
		return reader;
	}

	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}



}
