package unimib.ilaria.pigazzini.ir.project.indexing;

public enum FieldEnum {
	FIELD_EVENT_ID("event_id"),
	FIELD_EVENT_NAME("event_name"),
	FIELD_EVENT_DESCRIPTION("event_description"),
	FIELD_USER_CONTENT("user_content"),
	FIELD_USER_CATEGORY("user_category");
	
	//edge labels
	private final String _property;
	/**
     * @param text
     */
    private FieldEnum(final String property) {
    	_property = property;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return _property;
    }
}
