package unimib.ilaria.pigazzini.ir.project.indexing;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.WordlistLoader;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.it.ItalianLightStemFilter;
import org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter;
import org.apache.lucene.analysis.pattern.PatternReplaceCharFilter;
import org.apache.lucene.analysis.pattern.PatternReplaceFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.apache.lucene.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventAnalyzer extends StopwordAnalyzerBase {
	private final Logger logger = LoggerFactory.getLogger(EventAnalyzer.class);

	private CharArraySet stemExclusionSet;
	public final static String DEFAULT_STOPWORD_FILE = "italianStopwords.txt";

	private final Pattern urlRegex = Pattern.compile(
			"(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})");
	private final Pattern numRegex = Pattern.compile("[0-9]*");
	private final Pattern priceRegex = Pattern.compile("^[0-9]*[\\,\\.h][0-9]*$");
	private final Pattern ipRegex = Pattern.compile("([0-9]*\\.[0-9]*)*");
	private final Pattern unicodeRegex = Pattern.compile("\\\\u[0-9a-z]{4}");
	private static final CharArraySet DEFAULT_ARTICLES = CharArraySet
			.unmodifiableSet(new CharArraySet(Arrays.asList("c", "l", "all", "dall", "dell", "nell", "sull", "coll",
					"pell", "gl", "agl", "dagl", "degl", "negl", "sugl", "un", "m", "t", "s", "v", "d"), true));

	private static final CharArraySet stopwords2 = CharArraySet
			.unmodifiableSet(new CharArraySet(Arrays.asList("https", "http", ""), true));
	private final String italianStopwordsFile = "italianStopwords.txt";
	private final String englishStopwordsFile = "englishStopwords.txt";

	public CharArraySet getDefaultStopSet() {
		return getStopWordsFromFile(DEFAULT_STOPWORD_FILE);
	}

	private static class DefaultSetHolder {
		static final CharArraySet DEFAULT_STOP_SET;

		static {
			EventAnalyzer ev = new EventAnalyzer();
			
			DEFAULT_STOP_SET = ev.getStopWordsFromFile(DEFAULT_STOPWORD_FILE);
		}
	}

	public EventAnalyzer() {
		    this(DefaultSetHolder.DEFAULT_STOP_SET);
		  }

	public EventAnalyzer(CharArraySet stopwords) {
		this(stopwords, CharArraySet.EMPTY_SET);
	}

	public EventAnalyzer(CharArraySet stopwords, CharArraySet stemExclusionSet) {
		super(stopwords);
		this.stemExclusionSet = CharArraySet.unmodifiableSet(CharArraySet.copy(stemExclusionSet));
	}
	
	
	@Override
	protected Reader initReader(String fieldName, Reader reader) {
		Reader newReader = new PatternReplaceCharFilter(urlRegex, "", reader);
		newReader = new PatternReplaceCharFilter(numRegex, "", newReader);
		newReader = new PatternReplaceCharFilter(priceRegex, "", newReader);
		newReader = new PatternReplaceCharFilter(ipRegex, "", newReader);		
		newReader = new PatternReplaceCharFilter(unicodeRegex, "", newReader);
		return newReader;
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {	
		final Tokenizer source = new StandardTokenizer();
		TokenStream result = new StandardFilter(source);
		result = new PatternReplaceFilter(result, urlRegex, "", true);
		result = new PatternReplaceFilter(result, unicodeRegex, "", true);
		result = new PatternReplaceFilter(result, numRegex, "", true);
		result = new PatternReplaceFilter(result, priceRegex, "", true);
		result = new PatternReplaceFilter(result, ipRegex, "", true);
		result = new ElisionFilter(result, DEFAULT_ARTICLES);
		result = new LowerCaseFilter(result);
		result = new StopFilter(result, stopwords);
		result = new StopFilter(result, stopwords2);
		result = new StopFilter(result, this.getStopWordsFromFile(italianStopwordsFile));
		result = new StopFilter(result, this.getStopWordsFromFile(englishStopwordsFile));

		if (!stemExclusionSet.isEmpty())
			result = new SetKeywordMarkerFilter(result, stemExclusionSet);
		result = new ItalianLightStemFilter(result);
		return new TokenStreamComponents(source, result);
	}

	@Override
	protected TokenStream normalize(String fieldName, TokenStream in) {
		TokenStream result = new StandardFilter(in);
		result = new ElisionFilter(result, DEFAULT_ARTICLES);
		result = new LowerCaseFilter(result);
		return result;
	}

	private CharArraySet getStopWordsFromFile(String file) {
		try {
			// return
			// WordlistLoader.getSnowballWordSet(IOUtils.getDecodingReader(getClass(),
			// file, StandardCharsets.UTF_8));
			return WordlistLoader.getSnowballWordSet(IOUtils
					.getDecodingReader(getClass().getClassLoader().getResourceAsStream(file), StandardCharsets.UTF_8));
		} catch (IOException ex) {
			// default set should always be present as it is part of the
			// distribution (JAR)
			throw new RuntimeException("Unable to load default stopword set");
		}
	}

}
