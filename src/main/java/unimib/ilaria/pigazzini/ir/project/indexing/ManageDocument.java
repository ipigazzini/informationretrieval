package unimib.ilaria.pigazzini.ir.project.indexing;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

public class ManageDocument {
	private String event_id = "";
	private String event_name = "";
	private String event_description = "";
	
	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}



	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}



	public void setEvent_description(String event_description) {
		this.event_description = event_description;
	}



	public ManageDocument() {
		
	}
	

	
	public Document createDocument(String... fields) {
		Document doc = new Document();
		
		doc.add(new StringField(fields[0], fields[1], Field.Store.YES));
		
		for(int i = 2; i < fields.length; i=i+2) {
			doc.add(new TextField(fields[i], fields[i+1], Field.Store.YES));
		}
		
		return doc;
	}
}
