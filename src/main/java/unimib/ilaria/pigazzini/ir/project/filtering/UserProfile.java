package unimib.ilaria.pigazzini.ir.project.filtering;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.util.BytesRef;

public class UserProfile {
	private List<BytesRef> ciboTerms = new ArrayList<>();

	public UserProfile() {
		setCiboBag();
	}
	
	private void setCiboBag() {		
		ciboTerms.add(new BytesRef("vegan"));
		ciboTerms.add(new BytesRef("vegetal"));
		ciboTerms.add(new BytesRef("animal"));
		ciboTerms.add(new BytesRef("alimentazion"));
		ciboTerms.add(new BytesRef("mangiar"));
		//ciboTerms.add(new BytesRef("cibo"));
		ciboTerms.add(new BytesRef("latte"));
		ciboTerms.add(new BytesRef("prodott"));
		ciboTerms.add(new BytesRef("vegetarian"));
	}
	
	public List<BytesRef> getCiboTerms(){
		return this.ciboTerms;
	}
	
}
