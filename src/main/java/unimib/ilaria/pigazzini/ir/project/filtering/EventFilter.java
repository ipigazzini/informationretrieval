package unimib.ilaria.pigazzini.ir.project.filtering;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.BytesRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import unimib.ilaria.pigazzini.ir.project.IndexCreationTest;
import unimib.ilaria.pigazzini.ir.project.indexing.FieldEnum;

public class EventFilter {
	private final Logger logger = LoggerFactory.getLogger(EventFilter.class);

	private IndexSearcher searcher;
	
	public EventFilter(IndexSearcher searcher) {
		this.searcher = searcher;
	}
	

	
	public List<Document> search(Query query) throws IOException {
		List<Document> filteredEvents = new ArrayList<>();
		TopDocs topDocs = searcher.search(query, 50);
		ScoreDoc[] results = topDocs.scoreDocs;
		logger.info("total hits: " + topDocs.totalHits);
		
		for(int i = 0; i < results.length; ++i) {
			Document event = searcher.doc(results[i].doc);
			filteredEvents.add(event);
			logger.debug("***Document Score***" + results[i]);
			//logger.debug("Document: " + event);
			
			String eventName = event.getField(FieldEnum.FIELD_EVENT_NAME.toString()).stringValue();
			logger.debug("Nome evento: " + eventName);
		}
		
		return filteredEvents;
	}
	
	public void removeDuplicated(List<Document> docs) {
		//TreeSet<Document> outputList = new TreeSet<Document>(Comparator.comparing(p->p.getField(FieldEnum.FIELD_EVENT_NAME.toString()))); outputList.addAll(docs);
//		List<Document> unique = docs.stream()
//		                                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparing(doc->doc.getField(FieldEnum.FIELD_EVENT_NAME.toString())))),
//		                                                           ArrayList::new));
	}

	
	
}
