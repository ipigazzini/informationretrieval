# README #

**Quick summary**

* Recommendation system for Facebook events

**Version**

* 1.0.0

***Gathering and Indexing***

The collection of Facebook events used for this project was retrieved and stored in JSON format thanks to the Facebook API provided by a Python library (See...). The crawling phase was conducted on events located in Milano, Bergamo and Como in November 2017. The analysis radius scan was set to 500 meters and set in the geographical coordinates per each city. The total number of collected events was 817, in particular 333 from Milano, 132 from Bergamo and 352 from Como.
Once the gathering was concluded, the index was generated thanks to the Lucene library. This phase is constituted by three parts: the events acquisition, the analysis of the text of the events and the creation of the index. The key element of this phase was the redefinition of the analyzer. The index creation step is not reported since automatically supported by the class IndexWriter. 

***Events' Collection and Representation***
The events retrieved from Facebook was stored in JSON format by the Python library. The input stream of events was managed by a Java library named "json", in order to efficiently access every object of the JSON file. Then the events were represented as Lucene documents with their field. In the final structure, every document has field:

* event_id, which stores the Facebook ID assigned to the event;
* event_name, the name of the event;
* event_description, which stores the body of the event with the maximum amount of content.

***The Analyzer***

Lucene allows to define customized Analyzers. In this project, custom class is called EventAnalyzer and inherits from StopwordAnalyzerBase , a base class for Analyzers that need to make use of
stopword sets.

Every Analyzer is made up by three components: a chain of CharFilters, useful to analyze the input text stream of events before the tokenization phase; a Tokenizer, which identifies and partitions the tokens of the text stream; eventually a set of TokenFilter to further analyze the tokens produced by the Tokenizer. 
The class EventAnalyzer implements its own version of them. It concatenates three Char Filter in order to remove URLs, numbers and unicode special characters. This is vital since those patterns introduce noise in the final index when included and do not represent the actual content of the events.
For what concern the Tokenizer, it is an instance of StandardTokenizer, and breaks the input text basing on the Word Break rules from the Unicode Text Segmentation algorithm (See http://unicode.org/reports/tr29/).
Finally, this analyzer applies a set of filters to normalize the tokens and remove the stopwords for both italian and english language, since the gathered Facebook events are written in such languages.

***Warning***
The stop words removal has an inner fault since the events' descriptions are often written in both italian and english and the remover does not distinguish between the two type of text. For instance, the word "the" could indicate a hot drink in italian, but is considered a stop word in english: it will be removed in any case.

### Who do I talk to? ###

**Owner**
* Ilaria Pigazzini
**Contacts**
* i.pigazzini@campus.unimib.it